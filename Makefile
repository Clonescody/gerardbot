COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m

# If running on macOS, replace the next two lines
#LOCAL_USER_ID=1010
#LOCAL_GROUP_ID=1010
LOCAL_USER_ID=$(shell id -u)
LOCAL_GROUP_ID=$(shell id -g)

DOCKER_RUN_DEV=docker run --rm --env-file=dev.env --name=gerardbot -v `pwd`/src:/usr/src/app gerardbot/node-service-dev

.PHONY: build
build:
	docker build -t gerardbot/node-service docker/images/final

.PHONY: build-dev
build-dev:
	docker build --build-arg LOCAL_USER_ID=${LOCAL_USER_ID} --build-arg LOCAL_GROUP_ID=${LOCAL_GROUP_ID} -t gerardbot/node-service-dev docker/images/dev

.PHONY: start
start: build
	docker run -d --rm --env-file=dev.env --name=gerardbot gerardbot/node-service

.PHONY: start-dev
start-dev: build-dev npm-install-dev
	${DOCKER_RUN_DEV} npm run-script start-dev

.PHONY: start-final
start-final: build
	docker run -d --rm --env-file=.env --name=gerardbot gerardbot/node-service

.PHONY: start-command
start-command: build-dev npm-install-dev
	${DOCKER_RUN_DEV} npm run-script start-command

.PHONY: run-sandbox
run-sandbox: build-dev npm-install-dev
	${DOCKER_RUN_DEV} npm run-script sandbox

.PHONY: run-dev-purposes
run-dev-purposes: build-dev
	${DOCKER_RUN_DEV} sleep infinity

.PHONY: npm-install-dev
npm-install-dev:
	${DOCKER_RUN_DEV} npm install --prod

.PHONY: exec
exec:
	docker exec -ti gerardbot /bin/bash

.PHONY: stop
stop:
	docker stop gerardbot

.PHONY: logs
logs:
	docker logs -f gerardbot

.PHONY: push
push:
ifndef REGISTRY
	@echo "$(ERROR_COLOR)Need registry$(NO_COLOR)"
	@echo "$(ERROR_COLOR)Try: \"REGISTRY=my.registry make push\"$(NO_COLOR)"
	@exit
endif
ifdef REGISTRY
	echo "$(OK_COLOR)Registry: ${REGISTRY}$(NO_COLOR)"
	docker tag gerardbot/node-service ${REGISTRY}/gerardbot/node-service:test
	docker push ${REGISTRY}/gerardbot/node-service:test
endif
