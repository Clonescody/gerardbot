var config = {};
config.environment = process.env.ENVIRONMENT;
config.bot_api_key = process.env.BOT_API_KEY;
config.botname = process.env.BOT_NAME;
config.channel = process.env.CHANNEL;
config.text = process.env.TEXT;
config.ignoredModules = [
  "jtebaise",
  "restaurant",
  "smmry",
  "rousses",
  "helloworld",
  "wickedweasel",
  "deuxminutes",
  "blagues"
];
module.exports = config;
