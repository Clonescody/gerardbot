("use strict");
require("dotenv").load();
var config = require("../config.js");
var GerardBotCommand = require("../lib/gerardbotCommand.js");
var gerardbot = new GerardBotCommand({
  environment: config.environment,
  token: config.bot_api_key,
  name: config.botname,
  ignoredModules: config.ignoredModules,
  channel: config.channel,
  text: config.text,
});

gerardbot.run();
