let Module = function Constructor(bot) {
  this.data = null;
  this.config = null;
  this.keywords = [];
  this.bot = bot;
};

Module.prototype = {
  loadConfig: function (modulePath) {
    this.config = require(`${modulePath}/config.js`);
  },

  isMessageForMe: function () {
    let isForMe = false;
    let keysLength = this.config.keywords.length;

    for (let i = 0; i < keysLength; i++) {
      if (this.data.text.indexOf(this.config.keywords[i]) > -1) {
        isForMe = true;
        break;
      }
    }

    return isForMe;
  },

  getAnswer: function () {
    try {
      this.getAnswerCatchable();
    } catch (err) {
      console.log(err)
      this.bot.postMessage(
        this.data.channel,
        this.bot.getCurrentEnv() === "DEV"
            ? `Error : \n${err}`
            : `An Error occured, please check the server logs.`,
        this.bot.params
      );
    }
  },

  getAnswerCatchable: function () {
    throw 'method "getAnswerCatchable" not implemented.'
  },

  manage: function () {
    if (this.isMessageForMe() === true) {
      this.getAnswer();
    }
  },

  setData: function (data) {
    this.data = data;
  },

  // Ovedrride this in your module to specify the format of
  // your commands in the help command
  getModuleCommands: function () {
    return `- ${this.config.keywords.join(" / ")} \n`;
  },

  postMessageSelfChannel: function (text) {
    this.bot.postMessage(
      this.data.channel,
      text,
      this.bot.params
    );
  }
};

module.exports = Module;
