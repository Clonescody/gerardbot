("use strict");
require("dotenv").load();
var config = require("../config.js");
var GerardBot = require("../lib/gerardbot.js");
var gerardbot = new GerardBot({
  environment: config.environment,
  token: config.bot_api_key,
  name: config.botname,
  ignoredModules: config.ignoredModules
});

gerardbot.run();
