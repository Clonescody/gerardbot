const request = require("request");

class ImagesHelper {
  constructor() {
    this.apiUrl = `https://images.6emesens.org`;
    this.availableTypes = ["json"];
    this.defaultResponseType = "json";
  }

  retreiveStandardUrl(type, url, size) {
    // TODO: Manage more types if needed, json is perfect for slackbots
    let result = null;
    switch (type) {
      case "json":
        url = `${this.apiUrl}?json=${encodeURI(url)}`;
      default:
        return this.retreiveStandardUrlAsJson(url);
    }
  }

  retreiveStandardUrlAsJson(url) {
    return new Promise(function (resolve, reject) {
      request({ url }, function (err, response, body) {
        err && reject(err);
        const respJson = JSON.parse(response.body);
        const finalResult = respJson.url
          ? {
            status: 1,
            value: encodeURI(respJson.url)
          }
          : {
            status: 0,
            value: encodeURI(url)
          };
        resolve(finalResult);
      });
    });
  }
}

module.exports = ImagesHelper;
