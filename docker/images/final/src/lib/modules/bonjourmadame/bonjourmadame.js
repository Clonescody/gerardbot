const util = require("util");
const request = require("request");
const cheerio = require("cheerio");
const Module = require("./../../../bin/module.js");
const ImagesHelper = require("../../helpers/6emesens/images");
const imagesHelper = new ImagesHelper();
let BonjourMadame = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
};
util.inherits(BonjourMadame, Module);

BonjourMadame.prototype.getAnswerCatchable = function () {
  const self = this;
  request(
    {
      url: `https://bonjourmadame.6emesens.org/json`
    },
    function (err, response, body) {
      if (err === null) {
        const respJson = JSON.parse(response.body);
        self.postMessageSelfChannel(encodeURI(respJson.url));
      } else {
        request(
          {
            url: `http://www.bonjourmadame.fr/`
          },
          function (err, response, body) {
            if (err) { throw err; }
            $ = cheerio.load(body);
            const imgObj = $(".post-content > p img");
            let picture = $(imgObj).attr("src");
            if (picture.length) {
              imagesHelper
                .retreiveStandardUrl("json", picture)
                .then(result => self.postMessageSelfChannel(`${result.value}`));
            } else {
              self.postMessageSelfChannel(`Impossible de dire bonjour :(`)
            }

          }
        );
      }
    }
  );
};

module.exports = BonjourMadame;
