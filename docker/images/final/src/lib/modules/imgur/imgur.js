const util = require("util");
const request = require("request");
const Module = require("./../../../bin/module.js");
const ImagesHelper = require('../../helpers/6emesens/images')
const imagesHelper = new ImagesHelper();
let Imgur = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
  this.config = null;
};
util.inherits(Imgur, Module);

Imgur.prototype.getModuleCommands = function () {
  return `- ${this.config.keywords.join(" / ")} <keyword> \n`;
};

Imgur.prototype.getAnswerCatchable = function () {
  this.getImage(this.data);
};

Imgur.prototype.selectSubKey = function (data) {
  let sub = [];
  for (let i = 0, len = this.config.keywords.length; i < len; i++) {
    let regex = new RegExp(this.config.keywords[i] + "(.*)$");
    let arrMatches = data["text"].match(regex);
    if (arrMatches != null && arrMatches[1] !== null) {
      let results = arrMatches[1].trim().match(/[A-z]+/);
      if (
        results !== undefined &&
        Array.isArray(results) &&
        results[0] != undefined
      ) {
        sub.push(results[0]);
      }
    }
  }
  return sub;
};

Imgur.prototype.getImage = function (data) {
  let self = this;
  let subKey = self.selectSubKey(data);
  let window_ = ["day", "week", "month", "year", "all"];
  window_ = window_[self.getRandomIntInclusive(0, window_.length - 1)];
  let page = self.getRandomIntInclusive(1, 3);
  let sort = ["time", "top"];
  sort = [self.getRandomIntInclusive(0, sort.length - 1)];
  let sub = subKey[0];
  request(
    {
      url: `https://api.imgur.com/3/gallery/r/${sub}/${sort}/${window_}/${page}`,
      headers: {
        Authorization: `Client-ID ${self.config.imgurAuth}`
      },
      json: true
    },
    function (err, response, body) {
      if (err) { throw err; }
      const index = self.getRandomIntInclusive(0, body.data.length - 1);
      let result = body.data[index];
      if (index !== undefined && result !== undefined) {
        let url = result.link;
        if (result.type === 'video/mp4' && result.gifv) {
          url = result.gifv;
          self.postMessageSelfChannel(url);
        } else {
          imagesHelper
            .retreiveStandardUrl('json', url)
            .then(result => self.postMessageSelfChannel(`${result.value}`));
        }
      } else {
        self.postMessageSelfChannel(`Désolé je n'ai pas ça ¯\\_(ツ)_/¯`)
      }
    }
  );
};

Imgur.prototype.getRandomIntInclusive = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

module.exports = Imgur;
