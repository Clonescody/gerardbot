let util = require("util");
let request = require("request");
let cheerio = require("cheerio");
let Module = require("./../../../bin/module.js");

let Blagues = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
};
util.inherits(Blagues, Module);

Blagues.prototype.getAnswer = function () {
  const self = this;
  const totalPages = 424;
  const selectedPage = Math.round(Math.random() * totalPages);
  let joke = "";

  try {
    request(
      {
        url: `https://www.humour.com/blagues/?pg=${selectedPage}`
      },
      function (err, response, body) {
        $ = cheerio.load(body);
        const jokes = $(".result-blague > p");
        const jokeElt = $(
          jokes[Math.floor(Math.random() * (jokes.length - 1))]
        );
        jokeElt.contents().length > 2
          ? jokeElt.contents().each(function (i, elt) {
            joke += elt.name === "br" ? "\n" : `> ${$(this).text()}`;
          })
          : (joke = `> ${jokeElt.text()}`);
        self.bot.postMessage(
          self.data.channel,
          joke === "" ? "Le blaguobox est vide." : joke,
          self.bot.params
        );
      }
    );
  } catch (err) {
    self.bot.postMessage(
      self.data.channel,
      self.bot.getCurrentEnv() === "DEV"
        ? `Error : \n${err}`
        : `An Error occured, please check the server logs.`,
      self.bot.params
    );
  }
};

module.exports = Blagues;
