const util = require("util");
const request = require("request");
const cheerio = require("cheerio");
const Module = require("./../../../bin/module.js");
const ImagesHelper = require('../../helpers/6emesens/images')
const imagesHelper = new ImagesHelper();

let Rule34 = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
  this.baseUrl = `https://rule34.xxx/`;
};
util.inherits(Rule34, Module);

Rule34.prototype.getModuleCommands = function () {
  return `- ${this.config.keywords.join(" / ")} <keyword> | For everything, there's a porn version \n`;
};

Rule34.prototype.selectSubKey = function (data) {
  let sub = "";
  const keywords = data["text"].split(" ");
  for (let i = 2; i < keywords.length; i++) {
    sub += keywords.length - 1 === i ? keywords[i] : `${keywords[i]}_`
  }
  return sub;
};

Rule34.prototype.getAnswerCatchable = function () {
  let self = this;
  const randPage = self.getRandomIntInclusive(0, 10 * 42)
  const tag = self.selectSubKey(self.data);
  request(
    {
      url: `${this.baseUrl}index.php?page=post&s=list&tags=${tag}`
    },
    function (err, response, body) {
      if (err) { throw err; }
      const $ = cheerio.load(body);
      const imgThumbnails = $(".thumb > a", ".content");
      let url = `${self.baseUrl}${imgThumbnails[self.getRandomIntInclusive(0, imgThumbnails.length)].attribs["href"]}`;
      if (url !== undefined) {
        request(
          {
            url: `${url}&pid=${randPage}`
          },
          function (err, response, body) {
            if (err) { throw err; }
            const $ = cheerio.load(body);
            const imgObj = $("img", ".content")[0];
            let url = imgObj.attribs["data-cfsrc"];
            if (url !== undefined) {
              imagesHelper
                .retreiveStandardUrl('json', url)
                .then(result => self.postMessageSelfChannel(`${result.value}`));
            } else {
              self.postMessageSelfChannel(`Désolé je n'ai pas ça ¯\\_(ツ)_/¯`)
            }
          }
        );
      } else {
        self.postMessageSelfChannel(`Désolé je n'ai pas ça ¯\\_(ツ)_/¯`)
      }
    }
  );
};

Rule34.prototype.getRandomIntInclusive = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

module.exports = Rule34;
