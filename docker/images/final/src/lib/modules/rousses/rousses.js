let util = require("util");
let request = require("request");
let Module = require("./../../../bin/module.js");

let Rousses = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
};
util.inherits(Rousses, Module);

Rousses.prototype.getAnswer = function () {
  let boards = ["Bobiack03/rousses-sexy", "villongil/redhead"],
    board = boards[Math.floor(Math.random() * (boards.length - 1))];
  let self = this;
  request(
    {
      url:
        "https://api.pinterest.com/v1/boards/" +
        board +
        "/pins/?access_token=AY9lOLKZ9JDvHdCkcGgmdJ5Oi3MjFF0xI9nVBFZDNmxAS4AplAAAAAA&fields=image"
    },
    function (err, response, body) {
      if (err) {
        console.log(err);
        return;
      }

      let response = JSON.parse(body),
        pictures = response.data,
        picture =
          pictures[Math.floor(Math.random() * (pictures.length - 1))].image
            .original.url;

      self.bot.postMessage(
        self.data.channel,
        encodeURI(picture),
        self.bot.params
      );
    }
  );
};

module.exports = Rousses;
