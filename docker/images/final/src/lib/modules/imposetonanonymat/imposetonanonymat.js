const util = require("util");
const request = require("request");
const cheerio = require("cheerio");
const Module = require("./../../../bin/module.js");
const ImagesHelper = require("../../helpers/6emesens/images");
const imagesHelper = new ImagesHelper();
let Imposetonanonymat = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
};
util.inherits(Imposetonanonymat, Module);

Imposetonanonymat.prototype.getAnswerCatchable = function () {
  let self = this;
  let totalPages = 788;
  let selectedPage = Math.round(Math.random() * totalPages);
  request(
    {
      url: `http://www.imposetonanonymat.com/page/${selectedPage}`
    },
    function (err, response, body) {
      if (err) { throw err; }
      $ = cheerio.load(body);
      let pictures = $(".photo > img");
      if (pictures.length) {
        const picture = $(
          pictures[Math.floor(Math.random() * (pictures.length - 1))]
        ).attr("src");
        imagesHelper
          .retreiveStandardUrl("json", picture)
          .then(result =>
            self.postMessageSelfChannel(`${result.value}`)
          );
      } else {
        self.postMessageSelfChannel(`Ca va pas nan ?`)
      }
    }
  );
};

module.exports = Imposetonanonymat;
