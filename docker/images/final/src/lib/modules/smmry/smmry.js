let util = require("util");
let request = require("request");
let Module = require("./../../../bin/module.js");

let Smmry = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
  this.config = null;
};
util.inherits(Smmry, Module);



Smmry.prototype.getAnswer = function () {
  let url = this.extractUrl(this.data);
  this.summarize(url);
};

Smmry.prototype.summarize = function (url) {
  const apiKey = this.config.smmry_api_key;
  const sentences = this.config.sentence_count;
  let tldr = "";
  let self = this;
  request(
    {
      url:
        "http://api.smmry.com?SM_WITH_BREAK&SM_API_KEY=" +
        apiKey +
        "&SM_LENGTH=" +
        sentences +
        "&SM_URL=" +
        url
    },
    function (err, response, body) {
      if (err) {
        console.log(err);
        return;
      }

      tldr = JSON.parse(body);
      if (tldr["sm_api_content"] === undefined) {
        let text = "Sorry I have no credit left for today :(";
      } else {
        let text =
          "Hey! here's a short version of this article in " +
          self.sentences +
          " sentences ! \n\n " +
          tldr["sm_api_content"];
      }

      console.log(tldr["sm_api_limitation"]);
      self.bot.postMessage(
        self.data.channel,
        text.replace(/\[BREAK\]/g, "\n"),
        self.bot.params
      );
    }
  );
};

Smmry.prototype.extractUrl = function (data) {
  let string = data["text"];
  getUrl = new RegExp(
    "(((http|https):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-])))",
    "g"
  );
  let urls = string.match(getUrl);

  if (urls.length === 0) {
    this.bot.postMessage(
      this.data.channel,
      "No url found, sorry !",
      this.bot.params
    );
    return;
  }

  return urls[0];
};

module.exports = Smmry;
