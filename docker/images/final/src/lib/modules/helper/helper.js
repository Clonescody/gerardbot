const util = require("util");
const fs = require("fs");
const Module = require("./../../../bin/module.js");

const Helper = function Constructor(bot) {
  this.bot = bot;
};
util.inherits(Helper, Module);

Helper.prototype.getAnswer = function() {
  let commands = "Gerardbot Helper \n";
  commands += this.getVersion();
  commands += this.getModulesCommands();
  this.bot.postMessage(this.data.channel, commands, this.bot.params);
};

Helper.prototype.getVersion = function() {
  return `Version : ${fs.readFileSync("./version.txt", "utf8", function(
    err,
    version
  ) {
    return version;
  })} \n`;
};

Helper.prototype.getModulesCommands = function() {
  const ignoredModules = this.bot.getIgnoredModules();
  let commands = "Liste des commandes :\n";
  this.bot
    .getModules()
    .forEach(loadedModule => (commands += loadedModule.getModuleCommands()));

  if (ignoredModules.length > 1 && this.bot.getCurrentEnv() === "DEV") {
    commands += "Modules ignorés :\n";
    ignoredModules.forEach(
      (ignoredModule, index) =>
        (commands += `${ignoredModule}${
          index === ignoredModules.length - 1 ? "" : "/"
        }`)
    );
  }
  return commands;
};

module.exports = Helper;
