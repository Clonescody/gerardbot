let util = require("util");
let request = require("request");
let Module = require("./../../../bin/module.js");



let Restaurant = function Constructor(bot) {
  this.bot = bot;
  this.data = null;
  this.config = null;
};
util.inherits(Restaurant, Module);

Restaurant.prototype.getAnswer = function () {
  this.getNearbyFood(this.data);
};

Restaurant.prototype.getNearbyFood = function (data) {
  let radius = this.config.radius;
  let OfficeGeolocation = this.config.myplace;
  let GoogleApiKey = this.config.googlemaps_api_key;
  if (radius === undefined || radius === null) {
    radius = 1000;
  }

  let placesJSON;
  let self = this;
  let propertiesObject = {
    location: OfficeGeolocation,
    radius: radius,
    types: "restaurant",
    key: GoogleApiKey,
    opennow: false,
    query: "restaurant"
  };

  request(
    {
      url: "https://maps.googleapis.com/maps/api/place/nearbysearch/json",
      qs: propertiesObject
    },
    function (err, response, body) {
      if (err) {
        console.log(err);
        return;
      }

      placesJSON = JSON.parse(body);

      //TODO: exclude pharmacy, retail store etc
      let randomFoodPlace =
        placesJSON.results[
        Math.floor(Math.random() * placesJSON.results.length)
        ];

      request(
        {
          url: "https://maps.googleapis.com/maps/api/place/details/json",
          qs: { placeid: randomFoodPlace["place_id"], key: GoogleApiKey }
        },
        function (err2, response2, body2) {
          let ChosenPlaceDetails = JSON.parse(body2);
          let string =
            "Je vous propose le " + ChosenPlaceDetails.result["name"];

          for (let prop in ChosenPlaceDetails.result) {
            if (prop == "rating") {
              string =
                string + " Noté " + ChosenPlaceDetails.result["rating"] + "/5";
            }
            if (prop == "formatted_phone_number") {
              string =
                string +
                " | " +
                ChosenPlaceDetails.result["formatted_phone_number"];
            }
          }
          string = string + " " + ChosenPlaceDetails.result["url"];
          self.bot.postMessage(self.data.channel, string, self.bot.params);
        }
      );
    }
  );
};

module.exports = Restaurant;
