"use strict";

let fs = require("fs");
let path = require("path");
let util = require("util");
let Bot = require("slackbots");
let request = require("request");

let GerardBot = function Constructor(settings) {
  this.settings = settings;
  this.settings.name = this.settings.name || "gerardbot";
  this.params = { as_user: true };
  this.modules = this.loadModules();
};
util.inherits(GerardBot, Bot);

GerardBot.prototype.run = function () {
  console.log("Gerardbot is running... \n");
  GerardBot.super_.call(this, this.settings);

  this.on("start", this._onStart);
  this.on("message", this._onMessage);
};

GerardBot.prototype._onMessage = function (data) {
  if (
    this._isMessage(data) &&
    this._isTheMessageForMe(data) &&
    !this._isItMe(data)
  ) {
    this.sendData(data);
  }
};

GerardBot.prototype._loadBotUser = function () {
  let self = this;
  this.user = this.users.filter(function (user) {
    return user.name === self.name;
  })[0];
};

GerardBot.prototype._isMessage = function (data) {
  return data.type === "message" && Boolean(data.text);
};

GerardBot.prototype._isChannelConversation = function (data) {
  return typeof data.channel === "string" && data.channel[0] === "C";
};

GerardBot.prototype._isItMe = function (data) {
  return data.username === this.user.id;
};

GerardBot.prototype._isTheMessageForMe = function (data) {
  console.log("Received command : ", data.text.toLowerCase())
  return data.text.toLowerCase().indexOf("@" + this.user.id.toLowerCase()) > -1;
};

GerardBot.prototype._onStart = function (data) {
  this._loadBotUser();
};

GerardBot.prototype.sendData = function (data) {
  for (let i = 0; i < this.modules.length; i++) {
    this.modules[i].setData(data);
    this.modules[i].manage();
  }
};

GerardBot.prototype.loadModules = function () {
  let modules = [];
  let modulesDirectories = this.getDirectories("modules");

  for (let i = 0; i < modulesDirectories.length; i++) {
    let moduleName = modulesDirectories[i];
    if (this.settings.ignoredModules.includes(moduleName)) {
      continue;
    }
    const modulePath = `${__dirname}/modules/${moduleName}`;
    let module = require(path.resolve(`${modulePath}/${moduleName}.js`));
    module = new module(this);
    module.loadConfig(`${modulePath}`);
    modules.push(module);
  }

  return modules;
};

GerardBot.prototype.getDirectories = function (srcpath) {
  srcpath = path.resolve(__dirname, srcpath);
  return fs.readdirSync(srcpath).filter(function (file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
};

GerardBot.prototype.getModules = function () {
  return this.modules;
};

GerardBot.prototype.getIgnoredModules = function () {
  return this.settings.ignoredModules;
};

GerardBot.prototype.getCurrentEnv = function () {
  return this.settings.environment;
};

module.exports = GerardBot;
