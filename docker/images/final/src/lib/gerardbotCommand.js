"use strict";

var fs = require("fs");
var path = require("path");
var util = require("util");
var Bot = require("slackbots");
var request = require("request");

var GerardBotCommand = function Constructor(settings) {
  this.settings = settings;
  this.settings.name = this.settings.name || "gerardbot";
  this.params = { as_user: true };
  this.modules = this.loadModules();
};
util.inherits(GerardBotCommand, Bot);

GerardBotCommand.prototype.run = function () {
  console.log("GerardBotCommand is running... \n");
  GerardBotCommand.super_.call(this, this.settings);

  this.on("start", this._onStart);

  let data = { text: this.settings.text, channel: this.settings.channel };
  this.sendData(data);

  setTimeout(function (obj) {
    console.log('Goodbye!');
    process.exit();
  }, 60000, this);
};

GerardBotCommand.prototype._loadBotUser = function () {
  var self = this;
  this.user = this.users.filter(function (user) {
    return user.name === self.name;
  })[0];
};

GerardBotCommand.prototype._onStart = function (data) {
  this._loadBotUser();
};

GerardBotCommand.prototype.sendData = function (data) {
  for (var i = 0; i < this.modules.length; i++) {
    this.modules[i].setData(data);
    this.modules[i].manage();
  }
};

GerardBotCommand.prototype.loadModules = function () {
  var modules = [];
  var modulesDirectories = this.getDirectories("modules");

  for (var i = 0; i < modulesDirectories.length; i++) {
    var moduleName = modulesDirectories[i];
    if (this.settings.ignoredModules.includes(moduleName)) {
      continue;
    }
    const modulePath = `${__dirname}/modules/${moduleName}`;
    let module = require(path.resolve(`${modulePath}/${moduleName}.js`));
    module = new module(this);
    module.loadConfig(`${modulePath}`);
    modules.push(module);
  }

  return modules;
};

GerardBotCommand.prototype.getDirectories = function (srcpath) {
  srcpath = path.resolve(__dirname, srcpath);
  return fs.readdirSync(srcpath).filter(function (file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
};

GerardBotCommand.prototype.getModules = function () {
  return this.modules;
};

GerardBotCommand.prototype.getIgnoredModules = function () {
  return this.settings.ignoredModules;
};

GerardBotCommand.prototype.getCurrentEnv = function () {
  return this.settings.environment;
};

module.exports = GerardBotCommand;
