# **GérardBot!**

Hey ! This is a very simple Slack bot based on the excellent [slackbots](https://www.npmjs.com/package/slackbots "slackbots") library allowing you to easily add your own services.

This version is the currently running image on 6emesens.slack.org.

---

#### <i class="icon-file"></i> Configuration

You'll need a Slack token (https://my.slack.com/services/new/bot) and a bot name. Please check the `example.env` file for the bot configuration.

Note : if you want to contribute to the project, we recommend creating a second one named differently to use in the `dev.env` file, so you can use another channel and keep your production instance running while testing.

## Installation

### With Docker

You can simply use the `make start` command. It will build and run the `gerardbot` image given a `dev.env` file.

### With Node and NPM

1. Download and install the latest Node.js version
2. Clone the repository **git clone git@gitlab.com:Clonescody/gerardbot.git**
3. Enter the repository `cd ~/gerardbot/src`
4. Install: `npm install`

### Makefile

Available make commands : 

- `build` => build the image
- `start` => run the image ( will also build first ) with the `dev.env` file
- `start-final` => run the image ( will also build first ) with the `.env` file ( for production only )
- `run-dev-purposes` => run the image ( will also build first ) with the `dev.env` file and `sleep infinity`
- `exec` => executes bash inside the container
- `logs` => displays container logs
- `stop` => stops the container
- `push` => build, tag and push the image to a given registry

---

#### <i class="icon-file"></i> Create a module

The real advantage of Gérardbot is that you can easily create your own modules which will be plugged to you original bot.
Let's create a simple HelloWorld module as an example :

src/lib/modules/helloworld/helloworld.js

    var util = require('util');
    var config = require('./config.js');
    var Module = require('./../../../bin/module.js');

    var Helloworld = function Constructor(bot) {
        this.bot = bot;
        this.data = null;
        this.keyWords = config.keywords;
    };
    util.inherits(Helloworld, Module);

    Helloworld.prototype.getAnswer = function() {
          this.bot.postMessage(this.data.channel, 'Hello world!', this.bot.params);
    };

The `postMessage()` function allows you to send strings or links. 

---

src/lib/modules/helloworld/config.js

    var config = {};  
    config.keywords = ['hello', 'bonjour']; // keywords to catch to trigger your module
    module.exports = config;

--- 

## Usage

### Usage with Docker

1. You can simply buid and run the service by running `make start` using a `dev.env` file ( use `start-final` to run with `.env` ).
2. You can use `docker ps -a` to check if the image is running under the name `gerardbot`
3. Connect to your slack
4. Invite your bot in the channel you want: /invite @yourbot
5. Say @yourbot hello !
6. Enjoy

### With Node and NPM

1. Run your application inside the `src/` folder : `npm start .env` || `npm start dev.env`
2. Connect to your slack
3. Invite your bot in the channel you want: /invite @yourbot
4. Say @yourbot hello !
5. Enjoy

## Deployment with Docker

Even easier, you can deploy the service the same way by running `make push`, the image will be tagged as `:test`, you can change it manualy in the file.

It's done, Gerard is running on your registry.

---

## CI/CD

The CI is now available for GitLab, check the `gitlab-ci.yml` to see the configuration, you should change the `remote add origin` to use your repository.

You must generate a new ssh keypair to allow the bot to push tags to your repository. ( `ssh-keygen -C gerardbot@gitlab.com -f gerardbot.gitlab` )

You can then add the public part of the key under the `Repository > Deploy Keys` pannel on Gitlab.
Note : don't forget to check the "Write access allowed" option.

You'll need to add those variables to your Environment variables under the CI/CD settings pannel.
- DOCKER_REGISTRY // Your docker registry
- DOCKER_USERNAME // Your docker login username
- DOCKER_PASSWORD // Your docker login password
- SSH_PRIVATE_KEY // Add the private part of the key   

The git tags will be formatted as follow : `v<year>.<week>.<tags count>`

---

##Examples

### Imgur Module example

lib/modules/imgur/config.js

    var config = {};

    config.keywords = ['montre moi des', 'sors nous une photo de'];
    config.imgurAuth = 'Client-ID xxxxxxxxxxx';

    config.subs = {
        'chiens': 'dogs',
        'chat': 'cats',
        'paresseux': 'sloths',
    }

    module.exports = config;

![Imgur Module example](documents/img/imgur/imgur_example.png)

### Restaurant Module example

lib/modules/restaurant/config.js

    var config = {};

    config.myplace = '50.654809,3.071461';
    config.googlemaps_api_key = 'xxxxxxxxxxxxxxx';
    config.radius = 1000;
    config.keywords = ['on mange ou', 'on mange où'];
    module.exports = config;

![Imgur Module example](documents/img/restaurant/restaurant_example.png)

## Troubleshooting

### HTTP Response to HTTPS Client

If the error `server gave HTTP response to HTTPS client` appears when pushing to a repository, you must update ( or create ) the `deamon.json` file.
The following line must be added in the file, then Docker must be restarted :

    {
        "insecure-registries": ["<your_registery>:<registery_port>"]
    }

Source : [Docker Documentation](https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-configuration-file)
Related topic : [Docker Github](https://github.com/docker/distribution/issues/1874)

### If using Docker Desktop 
Open it then go to the "Daemon" section ( Settings > Daemon on windows ), then add the repository in the "Insecure registeries" section.

#### Windows

Location : C:\ProgramData\docker\config\daemon.json

#### Linux

Location : /etc/docker/daemon.json